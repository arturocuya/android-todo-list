package pe.quality.todolist;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class EditarElemento extends AppCompatActivity {
    private static final String TAG = "EditarElemento";
    TextInputEditText editar_nombre_elemento;
    Button btnActualizarElemento;
    int ID_ELEMENTO;
    String ID_LISTA_PADRE;
    Elemento elemento;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_elemento);
        editar_nombre_elemento = findViewById(R.id.editar_nombre_elemento);
        btnActualizarElemento = findViewById(R.id.btn_actualizar_elemento);
        ID_ELEMENTO = getIntent().getIntExtra("EXTRA_ID_ELEMENTO",1);

        final DB db = DB.getInstance(this);

        btnActualizarElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        String nuevoNombre = editar_nombre_elemento.getText().toString();
                        ID_LISTA_PADRE = getIntent().getStringExtra("EXTRA_ID_PADRE");
                        elemento = new Elemento(ID_ELEMENTO,nuevoNombre,ID_LISTA_PADRE);
                        Log.d(TAG, "run: "+ID_ELEMENTO+"\n"+nuevoNombre+"\n"+ID_LISTA_PADRE);
                        db.elementoDao().update(elemento);
                    }
                });
                finish();
            }
        });
    }
}
