package pe.quality.todolist;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by Arturo Cuya on 11/01/2018.
 */

// Clase de la Room Database

@Database(entities = {Lista.class,Elemento.class},version = 6,exportSchema = false)

public abstract class DB extends RoomDatabase {
    private static DB instance;
    public abstract listaDao listaDao();
    public abstract elementoDao elementoDao();

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            // Se añadió a las elementos la columna color
            database.execSQL("ALTER TABLE elementos ADD COLUMN color");
        }
    };

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            // Se creó la tabla de elementos de cada lista
            database.execSQL("CREATE TABLE elementos(id INTEGER," +
                    "nombreElemento TEXT," +
                    "listaId INTEGER," +
                    "PRIMARY KEY(id)" +
                    "FOREIGN KEY (foreignKeys) REFERENCES listas(id)");
        }
    };

    static final Migration MIGRATION_3_4 = new Migration(3,4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE listas ALTER COLUMN id TEXT NOT NULL");
            //database.execSQL("ALTER TABLE elementos ALTER COLUMN id TEXT NOT NULL");
            database.execSQL("ALTER TABLE elementos ALTER COLUMN listaid TEXT");
            database.execSQL("CREATE INDEX listaId ON elementos(listaId)");
        }
    };

    static final Migration MIGRATION_4_5 = new Migration(4,5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE listas ALTER COLUMN id INTEGER PRIMARY KEY");
            database.execSQL("ALTER TABLE elementos ALTER COLUMN id INTEGER PRIMARY KEY");
        }
    };

    static final Migration MIGRATION_5_6 = new Migration(5,6) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE elementos ALTER COLUMN id INTEGER PRIMARY KEY");
        }
    };

    public static DB getInstance(Context context){
        return Room.databaseBuilder(context.getApplicationContext(),DB.class,"DB_v4")
                .allowMainThreadQueries()
                .build();
    }


}


