package pe.quality.todolist;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

// Clase del elemento de lista. Cada elemento está enlazado a una lista mediante la Foreign Key "listaId"

@Entity(tableName = "elementos",
        foreignKeys = @ForeignKey(
        entity = Lista.class,
        parentColumns = "id",
        childColumns = "listaId",
        onDelete = CASCADE),
        indices = @Index(value = "listaId"))

public class Elemento {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String nombreElemento;
    public final String listaId;

    public Elemento(int id, String nombreElemento, final String listaId){
        this.id = id;
        this.nombreElemento = nombreElemento;
        this.listaId = listaId;
    }


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getNombreElemento() {
        return nombreElemento;
    }
    public void setNombreElemento(String nombreElemento) {
        this.nombreElemento = nombreElemento;
    }

    public String getListaId() {
        return listaId;
    }

}
