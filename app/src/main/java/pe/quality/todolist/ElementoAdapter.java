package pe.quality.todolist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Arturo Cuya on 15/01/2018.
 */

class ElementoAdapter extends RecyclerView.Adapter<ElementoAdapter.ViewHolder> {  // Esta extensión es importante
    private static final String TAG = "ElementoAdapter";
    public List<Elemento> elementos;
    private Context context;

    public ElementoAdapter(List<Elemento> elementos, Context context) {   // Constructor de la clase
        this.elementos = elementos;
        this.context = context;
    }

    @Override
    public ElementoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vista_elemento_fila,parent,false);
        return new ViewHolder(view);
        // Esta función retorna una variable que contiene el archivo xml que será rellenado con datos
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Aquí se capturan las posiciones en donde se guardarán los datos de la DB
        public TextView elementoNombre;
        public LinearLayout contenedor;
        public ImageButton btnEditarElemento;
        public ImageButton btnEliminarElemento;
        public ViewHolder(View itemView) {
            super(itemView);
            elementoNombre = itemView.findViewById(R.id.nombreElemento);
            btnEditarElemento = itemView.findViewById(R.id.btnEditarElemento);
            btnEliminarElemento = itemView.findViewById(R.id.btnBorrarElemento);
            contenedor = itemView.findViewById(R.id.elementoContenedor);
        }
    }

    @Override
    public void onBindViewHolder(final ElementoAdapter.ViewHolder holder, final int position) {
        // Aquí se coloca en cada instancia del archivo xml los datos correspondientes de cada elemento de la DB
        holder.elementoNombre.setText(elementos.get(position).getNombreElemento());
        holder.btnEliminarElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DB db = DB.getInstance(context);
                db.elementoDao().deleteElemento(elementos.get(holder.getAdapterPosition()).getId());
                elementos.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            }
        });
        holder.btnEditarElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,EditarElemento.class);
                intent.putExtra("EXTRA_ID_ELEMENTO",elementos.get(holder.getAdapterPosition()).getId());
                intent.putExtra("EXTRA_ID_PADRE",elementos.get(holder.getAdapterPosition()).getListaId());
                Log.d(TAG, "ID TO KNOW: "+elementos.get(holder.getAdapterPosition()).getListaId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() { // Esto devuelve el número de elementos de la DB
        return elementos.size();
    }


}
