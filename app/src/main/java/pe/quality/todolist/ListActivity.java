package pe.quality.todolist;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

public class ListActivity extends Activity {
    int id_fila_aux;
    FloatingActionButton fab;
    TextView nombreFila;
    TextView fechaHoraFila;
    TextView ubicacionFila;

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String nombre_fila_aux = getIntent().getStringExtra("EXTRA_NOMBRE_FILA");
        String fh_fila_aux = getIntent().getStringExtra("EXTRA_FH_FILA");
        String ubicacion_fila_aux = getIntent().getStringExtra("EXTRA_UBICACION_FILA");

        id_fila_aux = getIntent().getIntExtra("EXTRA_ID_FILA",0);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        final DB db = DB.getInstance(this);
        List<Elemento> elementos = db.elementoDao().findElementFromList(String.valueOf(id_fila_aux));

        nombreFila = findViewById(R.id.listaNombreInside);
        fechaHoraFila = findViewById(R.id.listaFechaHoraInside);
        ubicacionFila = findViewById(R.id.listaUbicacionInside);
        recyclerView = findViewById(R.id.recycler_view);

        nombreFila.setText(nombre_fila_aux);
        fechaHoraFila.setText(fh_fila_aux);
        ubicacionFila.setText(ubicacion_fila_aux);

        // Estas tres lineas inicializan el RecyclerView, el adapter y los setean
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ElementoAdapter(elementos,this);
        recyclerView.setAdapter(adapter);

        fab = findViewById(R.id.listaFabInside);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AnadirElemento.class);
                intent.putExtra("EXTRA_ID_FILA",id_fila_aux);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        final DB db = DB.getInstance(this);
        List<Elemento> elementos = db.elementoDao().findElementFromList(String.valueOf(id_fila_aux));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ElementoAdapter(elementos,this);
        recyclerView.setAdapter(adapter);
    }
}
