package pe.quality.todolist;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

// Clase del objeto lista. Cada vez que una clase se marca con el tag @Entity, se sabe que cuando
// se instancie se añadirá a la base de datos.

@Entity (tableName = "listas")
public class Lista {
    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "lista_nombre")
    private  String listaNombre;

    @ColumnInfo(name = "lista_fechaHora")
    private  String listaFechaHora;

    @ColumnInfo(name = "lista_ubicacion")
    private  String listaUbicacion;

    @ColumnInfo(name = "color")
    private int color;


    public Lista(int id, String listaNombre, String listaFechaHora, String listaUbicacion, int color) {
        this.id = id;
        this.listaNombre = listaNombre;
        this.listaFechaHora = listaFechaHora;
        this.listaUbicacion = listaUbicacion;
        this.color = color;
    }

    public String getListaNombre() {
        return listaNombre;
    }

    public void setListaNombre(String listaNombre) {
        this.listaNombre = listaNombre;
    }

    public String getListaFechaHora() {
        return listaFechaHora;
    }

    public void setListaFechaHora(String listaFechaHora) {
        this.listaFechaHora = listaFechaHora;
    }

    public String getListaUbicacion() {
        return listaUbicacion;
    }

    public void setListaUbicacion(String listaUbicacion) {
        this.listaUbicacion = listaUbicacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
