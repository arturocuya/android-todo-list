package pe.quality.todolist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by Arturo Cuya on 11/01/2018.
 */

// El archivo dao es el que se encarga de acceder a los datos. Cada elemento distinto de la DB debe tener un DAO
// Dao : Data access object

@Dao
public interface listaDao {
    @Query("SELECT * FROM listas")
    List<Lista> getAllListas();

    @Insert
    void insertAll(Lista... listas);

    @Update
    void update(Lista... listas);

    @Delete
    void delete(Lista... listas);

    @Query("DELETE FROM listas WHERE id=:id")
    void deleteLista(final int id);
}
