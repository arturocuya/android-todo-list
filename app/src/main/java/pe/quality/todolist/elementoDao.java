package pe.quality.todolist;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface elementoDao {
    @Query("SELECT * FROM elementos WHERE listaId=:listaId")
    List<Elemento> findElementFromList(final String listaId);

    @Insert
    void insert(Elemento elemento);

    @Update
    void update(Elemento... elementos);

    @Query("DELETE FROM elementos WHERE id=:id")
    void deleteElemento(final int id);
}
