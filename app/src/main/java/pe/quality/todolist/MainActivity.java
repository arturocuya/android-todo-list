package pe.quality.todolist;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

// La actividad de la pantalla principal

public class MainActivity extends Activity {
    private static final String TAG = "MainActivity"; // Necesario para crear tags
    FloatingActionButton fab;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fab = findViewById(R.id.mainFab);
        recyclerView = findViewById(R.id.recycler_view);


        final DB db = DB.getInstance(this);
        List<Lista> listas = db.listaDao().getAllListas();

        // Estas tres lineas inicializan el RecyclerView, el adapter y los setean
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ListaAdapter(listas, this);
        recyclerView.setAdapter(adapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Al presionar el fab, la pantalla cambia a activity_anadir_lista.xml
                Intent intent = new Intent(MainActivity.this,AnadirLista.class);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        final DB db = DB.getInstance(this);
        db.listaDao().getAllListas();
        recyclerView.setAdapter(adapter);
    }
}
