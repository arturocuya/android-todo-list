package pe.quality.todolist;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class AnadirElemento extends AppCompatActivity {
    private static final String TAG = "AnadirElemento";
    TextInputEditText nombreElemento;
    Button anadirElemento;

    int id_fila_aux;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anadir_elemento);

        nombreElemento = findViewById(R.id.nombre_nuevo_elemento);
        anadirElemento = findViewById(R.id.btn_anadir_elemento);
        id_fila_aux = getIntent().getIntExtra("EXTRA_ID_FILA",1);

        final DB db = DB.getInstance(this);

        anadirElemento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Elemento elemento = new Elemento(0,nombreElemento.getText().toString(),String.valueOf(id_fila_aux));
                db.elementoDao().insert(elemento);
                finish();
                Log.d(TAG, "Se añadio elemento: "+
                "Nombre: "+nombreElemento.getText().toString()+"\n"+
                "Id del padre: "+String.valueOf(id_fila_aux));
            }
        });
    }
}
