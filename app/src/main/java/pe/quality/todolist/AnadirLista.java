package pe.quality.todolist;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

// Esta clase se encarga de añadir un nuevo objeto lista a la Room Database,
// y de la interfaz de la vista activity_anadir_lista.xml

public class AnadirLista extends AppCompatActivity {

    EditText nombre_nueva_lista;
    Button btn_anadir_lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anadir_lista);

        nombre_nueva_lista = findViewById(R.id.nombre_nueva_lista);
        btn_anadir_lista = findViewById(R.id.btn_anadir_lista);

        final DB db = DB.getInstance(this);

        btn_anadir_lista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {    // Acciones que realiza el botón btn_anadir_lista
                String time = new SimpleDateFormat("hh:mm:ss").format(System.currentTimeMillis());

                Lista lista = new Lista(0,
                        nombre_nueva_lista.getText().toString(),
                        DateFormat.getDateInstance().format(Calendar.getInstance().getTime()) + "\n" +
                                time,
                        "Ubicación respectiva", 1);
                db.listaDao().insertAll(lista);
                startActivity(new Intent(AnadirLista.this,MainActivity.class));
//              ^ Luego de añadir el nuevo elemento a la DB, la vista regresa a activity_main.xml
            }
        });
    }
}
