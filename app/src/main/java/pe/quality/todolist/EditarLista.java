package pe.quality.todolist;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class EditarLista extends AppCompatActivity {
    TextInputEditText editar_nombre_lista;
    Button btn_actualizar;
    int ID_LISTA;
    Lista lista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_lista);
        editar_nombre_lista = findViewById(R.id.editar_nombre_lista);
        btn_actualizar = findViewById(R.id.btn_actualizar);
        ID_LISTA = getIntent().getIntExtra("EXTRA_ID_FILA",0);
        final DB db = DB.getInstance(this);

        btn_actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        String time = new SimpleDateFormat("hh:mm:ss").format(System.currentTimeMillis());
                        String nuevoNombre = editar_nombre_lista.getText().toString();
                        String nuevoLFH = DateFormat.getDateInstance().format(Calendar.getInstance().getTime()) + "\n" + time;
                        lista = new Lista(ID_LISTA,nuevoNombre,nuevoLFH,"Ubicación actual",1);
                        db.listaDao().update(lista);
                    }
                });
                startActivity(new Intent(EditarLista.this,MainActivity.class));
            }
        });
    }
}
