package pe.quality.todolist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

// Adapter del objeto lista. Un adapter actúa como puente entre AdapterView y la base de datos de
// donde se extraen sus datos. Cada elemento de la base de datos debe tener su propio Adapter.

class ListaAdapter extends RecyclerView.Adapter<ListaAdapter.ViewHolder> {  // Esta extensión es importante
    private static final String TAG = "t";
    public List<Lista> listas;
    private Context context;

    public ListaAdapter(List<Lista> listas, Context context) {   // Constructor de la clase
        this.listas = listas;   // En esta lista se almacenarán las instancias de Lista,
                                // para luego ponerlas en la DB
        this.context = context;
    }

    @Override
    public ListaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vista_lista_fila,parent,false);
        return new ViewHolder(view);

        // Esta función retorna una variable que contiene el archivo xml que será rellenado con datos
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // Aquí se capturan las posiciones en donde se guardarán los datos de la DB
        public TextView listaNombre;
        public TextView listaFechaHora;
        public LinearLayout contenedor;
        public ImageButton editarLista;
        public ImageButton borrarLista;

        public ViewHolder(View itemView) {
            super(itemView);
            listaNombre = itemView.findViewById(R.id.listaNombre);
            listaFechaHora = itemView.findViewById(R.id.listaFechaHora);
            contenedor = itemView.findViewById(R.id.vistaFila);
            editarLista = itemView.findViewById(R.id.btnEditarFila);
            borrarLista = itemView.findViewById(R.id.btnBorrarFila);
        }
    }

    @Override
    public void onBindViewHolder(final ListaAdapter.ViewHolder holder, final int position) {
        // Aquí se coloca en cada instancia del archivo xml los datos correspondientes de cada elemento de la DB
        holder.listaNombre.setText(listas.get(position).getListaNombre());
        holder.listaFechaHora.setText(listas.get(position).getListaFechaHora());
        //holder.contenedor.setId(listas.get(position).getId());
        holder.contenedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,ListActivity.class);
                intent.putExtra("EXTRA_NOMBRE_FILA",
                        listas.get(holder.getAdapterPosition())
                                .getListaNombre());
                intent.putExtra("EXTRA_FH_FILA",
                        listas.get(holder.getAdapterPosition())
                                .getListaFechaHora());
                intent.putExtra("EXTRA_ID_FILA",
                        listas.get(holder.getAdapterPosition())
                                .getId());
                context.startActivity(intent);
            }
        });
        holder.editarLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,EditarLista.class);
                intent.putExtra("EXTRA_ID_FILA",
                        listas.get(holder.getAdapterPosition())
                                .getId());
                context.startActivity(intent);
            }
        });
        holder.borrarLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DB db = DB.getInstance(context);
                db.listaDao().deleteLista(listas.get(holder.getAdapterPosition()).getId());
                listas.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() { // Esto devuelve el número de elementos de la DB
        return listas.size();
    }


}
